
public class Poliwhirl extends Pokemon implements Skillwater,Skillnormal{
    public Poliwhirl(String name){
        super(name,500,400,5,"Water Gun","Hydro Pump","Aqua tail","Tackle");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        watergun(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        hydropump(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        aquatail(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        tackle(myPokemon,enemy);
    }

    //Water
    public void watergun(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
    }
    public void hydropump(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
    }
    
    public void aquatail(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
    }
    //Normal
    public void growl(Pokemon myPokemon,Pokemon enemy){}

    public void tackle(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
    }
}
