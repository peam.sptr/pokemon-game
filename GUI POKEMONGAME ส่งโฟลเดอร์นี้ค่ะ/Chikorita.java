
public class Chikorita extends Pokemon implements Skillgrass,Skillnormal{
    
    public Chikorita(String name){
        super(name,500,400,1,"Magical Leaf","Solar Beam","Tackle","Growl");
    }

    public void attack1(Pokemon myPokemon,Pokemon enemy){
        magicalleaf(myPokemon,enemy);
    }

    public void attack2(Pokemon myPokemon,Pokemon enemy){
        solarbeam(myPokemon,enemy);
    }

    public void attack3(Pokemon myPokemon,Pokemon enemy){
        tackle(myPokemon,enemy);
    }

    public void attack4(Pokemon myPokemon,Pokemon enemy){
        growl(myPokemon,enemy);
    }

    public void magicalleaf(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(20);
        }
    }

    public void solarbeam(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(50)){
            enemy.damage(25);
        }
    }

    public void tackle(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(25)){
            enemy.damage(15);
        }
    }

    public void growl(Pokemon myPokemon,Pokemon enemy){
        if(myPokemon.reduce(100)){
            enemy.damage(40);
        }
        
    }
}
