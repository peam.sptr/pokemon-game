
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.ArrayList;


public class GuiBattleGround extends JFrame{
    
    private JButton poke[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
    
    public GuiBattleGround(Trainer player,Pokemon myPokemon,Pokemon enemyPokemon,int number){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(1480,850);
        setResizable(false);
        setVisible(true);
      
        Container c = getContentPane();
        
        JLabel background = new JLabel("");
        
        ImageIcon bg1 = new ImageIcon("grassbattle.png");
        ImageIcon bg2 = new ImageIcon("firebattle.png");
        ImageIcon bg3 = new ImageIcon("cavebattle.png");
        ImageIcon bg4 = new ImageIcon("desertbattle.png");
        ImageIcon bg5 = new ImageIcon("snowbattle.png");
        
        if(number == 1){
            background.setIcon(bg1);
            background.setBounds(0, 0, 1480, 850);
        }
        if(number == 2){
            background.setIcon(bg2);
            background.setBounds(0, 0, 1480, 850);
        }
        if(number == 3){
            background.setIcon(bg3);
            background.setBounds(0, 0, 1480, 850);
        }
        if(number == 4){
            background.setIcon(bg4);
            background.setBounds(0, 0, 1480, 850);
        }
        if(number == 5){
            background.setIcon(bg5);
            background.setBounds(0, 0, 1480, 850);
        }
        c.add(background);
        pack();
        setLocationRelativeTo(null);
        
        ImageIcon bmypoke1 = new ImageIcon("bchikorita.png");
        ImageIcon bmypoke2 = new ImageIcon("bcyndaquil.png");
        ImageIcon bmypoke3 = new ImageIcon("btotodile.png");
        ImageIcon bmypoke4 = new ImageIcon("bpikachu.png");
        ImageIcon bmypoke5 = new ImageIcon("bpoliwhirl.png");
        ImageIcon fmypoke1 = new ImageIcon("fchikorita.png");
        ImageIcon fmypoke2 = new ImageIcon("fcyndaquil.png");
        ImageIcon fmypoke3 = new ImageIcon("ftotodile.png");
        ImageIcon fmypoke4 = new ImageIcon("fpikachu.png");
        ImageIcon fmypoke5 = new ImageIcon("fpoliwhirl.png");
           
        JLabel mypoke = new JLabel("");
        JLabel enemypoke = new JLabel("");
        
        if(myPokemon.getNumber() == 1){
            mypoke.setIcon(bmypoke1);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 2){
            mypoke.setIcon(bmypoke2);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 3){
            mypoke.setIcon(bmypoke3);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 4){
            mypoke.setIcon(bmypoke4);
            mypoke.setBounds(0,0,1480,850);
        }
        else if(myPokemon.getNumber() == 5){
            mypoke.setIcon(bmypoke5);
            mypoke.setBounds(0,0,1480,850);
        }
        
        if(enemyPokemon.getNumber() == 1){
            enemypoke.setIcon(fmypoke1);
            enemypoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 2){
            enemypoke.setIcon(fmypoke2);
            enemypoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 3){
            enemypoke.setIcon(fmypoke3);
            enemypoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 4){
            enemypoke.setIcon(fmypoke4);
            enemypoke.setBounds(0,0,1480,850);
        }
        else if(enemyPokemon.getNumber() == 5){
            enemypoke.setIcon(fmypoke5);
            enemypoke.setBounds(0,0,1480,850);
        }
        
        background.add(mypoke);
        background.add(enemypoke);
        
//        ImageIcon lucus = new ImageIcon("");
//        ImageIcon draw = new ImageIcon("leaf.gif");
        
//        JLabel trainer = new JLabel("");

//        if(player.getName().equals("Lucus")){
//            trainer.setIcon(lucus);
//            trainer.setBounds(20,240,300,300);
//        }
//        if(player.getName().equals("Draw")){
//            trainer.setIcon(draw);
//            trainer.setBounds(20,240,300,300);
//        }
        
//        background.add(trainer);
        
        JLabel text1 = new JLabel("Attack: ");
        JLabel text2 = new JLabel("Bag: ");
        JLabel text3 = new JLabel("Change Pokemon: ");
        JLabel text4 = new JLabel("Run");
        
        JButton skill1 = new JButton(myPokemon.getNameskill1());
        JButton skill2 = new JButton(myPokemon.getNameskill2());
        JButton skill3 = new JButton(myPokemon.getNameskill3());
        JButton skill4 = new JButton(myPokemon.getNameskill4());
        
        skill1.setToolTipText("Damage:20HP Mana:25MP");
        skill2.setToolTipText("Damage:25HP Mana:50MP");
        skill3.setToolTipText("Damage:15HP Mana:25MP");
        skill4.setToolTipText("Damage:40HP Mana:100MP");
        
        JLabel show1 = new JLabel("");
        JLabel show2 = new JLabel("");
        JLabel show3 = new JLabel("");
        JLabel show4 = new JLabel("");
        JLabel show5 = new JLabel("");
        JLabel show6 = new JLabel("");
        
        show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
        show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
        show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
        show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
        
        if(number == 3 || number == 2){
            show3.setForeground(Color.white);
            show4.setForeground(Color.white);
            show5.setForeground(Color.white);
            show6.setForeground(Color.white);
        }
        
        text1.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        text2.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        text3.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        text4.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        
        text1.setForeground(Color.white);
        text2.setForeground(Color.white);
        text3.setForeground(Color.white);
        text4.setForeground(Color.white);
        
        show3.setFont(new Font("Eras Demi ITC", Font.BOLD, 25));
        show4.setFont(new Font("Eras Demi ITC", Font.BOLD, 25));
        show5.setFont(new Font("Eras Demi ITC", Font.BOLD, 25));
        show6.setFont(new Font("Eras Demi ITC", Font.BOLD, 25));
        
        JButton berry = new JButton("Berry : " + player.getItem().getBerry());
        JButton potion = new JButton("Potion : " + player.getItem().getPotion());
        JButton ether = new JButton("Ether : " + player.getItem().getEther());
        JButton superether = new JButton("Superether : " + player.getItem().getSuperether());
        JButton pokeball = new JButton("Pokeball : " + player.getItem().getPokeball());
        
        int i = 0;
        int x = 220;
        for(Pokemon p : player.getBag()){
            if(p.getName().equals(myPokemon.getName())){
                poke[i].setEnabled(false);
            }
            poke[i].setText(" " + (i+1) + " ");
            poke[i].setToolTipText(p.getName() + " , HP:" + p.getHp() + "/" + p.getRememberhp() + " MP: " + p.getMp()+ "/" + p.getRemembermp());
            poke[i].setBounds(x, 760, 65, 25);
            background.add(poke[i]);
            i++;
            x += 65;
        }
        
        JButton run = new JButton("Click!");
        
        text1.setBounds(35, 700, 100, 25);
        text2.setBounds(35, 730, 100, 25);
        text3.setBounds(35, 760, 180, 25);
        text4.setBounds(35, 790, 100, 25);
        
        skill1.setBounds(115, 700, 130, 25);
        skill2.setBounds(245, 700, 130, 25);
        skill3.setBounds(375, 700, 130, 25);
        skill4.setBounds(505, 700, 130, 25);
        
        show1.setBounds(900, 700, 300, 50);
        show2.setBounds(900, 750, 300, 50);
        show3.setBounds(150, 400, 300, 50);
        show4.setBounds(700, 125, 300, 50);
        show5.setBounds(150, 450, 300, 50);
        show6.setBounds(700, 175, 300, 50);
        
        berry.setBounds(85, 730, 104, 25);
        potion.setBounds(189, 730, 104, 25);
        ether.setBounds(293, 730, 104, 25);
        superether.setBounds(397, 730, 114, 25);
        pokeball.setBounds(511, 730, 104, 25);
        
        run.setBounds(80, 790, 100, 25);
        
        background.add(text1);
        background.add(text2);
        background.add(text3);
        background.add(text4);
        
        background.add(skill1);
        background.add(skill2);
        background.add(skill3);
        background.add(skill4);
        
        background.add(show1);
        background.add(show2);
        background.add(show3);
        background.add(show4);
        background.add(show5);
        background.add(show6);
        
        background.add(berry);
        background.add(potion);
        background.add(ether);
        background.add(superether);
        background.add(pokeball);
        
        background.add(run);
        
        //skill        
        skill1.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               if(myPokemon.getMp() >= 25){
                    myPokemon.attack1(myPokemon,enemyPokemon);
                    show1.setText(myPokemon.getName() +" use skill "+ myPokemon.getNameskill1());
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());

                    if(enemyPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,myPokemon.getName() + " win , You got 500 coins","Message",JOptionPane.ERROR_MESSAGE);
                        player.getItem().setCoin(player.getItem().getCoin()+500);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                    
                    boolean loop = true;
                    
                    do{
                        int randomskill = (int)(Math.random() * 8);
                        if(randomskill == 0){
                            if(enemyPokemon.getMp() >= 25){
                                enemyPokemon.attack1(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 1){
                             if(enemyPokemon.getMp() >= 50){
                                enemyPokemon.attack2(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 2){
                             if(enemyPokemon.getMp() >= 25){
                             enemyPokemon.attack3(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 3){
                             if(enemyPokemon.getMp() >= 100){
                             enemyPokemon.attack4(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 4){
                             enemyPokemon.healHp(15);
                             show2.setText(enemyPokemon.getName() + "use berry");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 5){
                             enemyPokemon.healHp(25);
                             show2.setText(enemyPokemon.getName() + "use potion");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 6){
                             enemyPokemon.healMp(20);
                             show2.setText(enemyPokemon.getName() + "use ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 7){
                             enemyPokemon.healMp(30);
                             show2.setText(enemyPokemon.getName() + "use super ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                    }while(loop);

                    if(myPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                        setVisible(false);
                        new GuiPlay(player);
                    }
               }
               else{
                   JOptionPane.showMessageDialog(null,"Not enough Mana","Wraning",JOptionPane.ERROR_MESSAGE);
               }
           }
        });
        
        skill2.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               if(myPokemon.getMp() >= 50){
                    myPokemon.attack2(myPokemon,enemyPokemon);
                    show1.setText(myPokemon.getName() +" use skill "+ myPokemon.getNameskill2());
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());

                    if(enemyPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,myPokemon.getName() + " win , You got 500 coins","Message",JOptionPane.ERROR_MESSAGE);
                        player.getItem().setCoin(player.getItem().getCoin()+500);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                    
                    boolean loop = true;
                    
                    do{
                        int randomskill = (int)(Math.random() * 8);
                        if(randomskill == 0){
                            if(enemyPokemon.getMp() >= 25){
                                enemyPokemon.attack1(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 1){
                             if(enemyPokemon.getMp() >= 50){
                                enemyPokemon.attack2(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 2){
                             if(enemyPokemon.getMp() >= 25){
                             enemyPokemon.attack3(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 3){
                             if(enemyPokemon.getMp() >= 100){
                             enemyPokemon.attack4(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 4){
                             enemyPokemon.healHp(15);
                             show2.setText(enemyPokemon.getName() + "use berry");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 5){
                             enemyPokemon.healHp(25);
                             show2.setText(enemyPokemon.getName() + "use potion");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 6){
                             enemyPokemon.healMp(20);
                             show2.setText(enemyPokemon.getName() + "use ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 7){
                             enemyPokemon.healMp(30);
                             show2.setText(enemyPokemon.getName() + "use super ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                    }while(loop);

                    if(myPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                        setVisible(false);
                        new GuiPlay(player);
                    }
               }
               else{
                   JOptionPane.showMessageDialog(null,"Not enough Mana","Wraning",JOptionPane.ERROR_MESSAGE);
               }
           }
        });
        
        skill3.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               if(myPokemon.getMp() >= 25){
                    myPokemon.attack3(myPokemon,enemyPokemon);
                    show1.setText(myPokemon.getName() +" use skill "+ myPokemon.getNameskill3());
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());

                    if(enemyPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,myPokemon.getName() + " win , You got 500 coins","Message",JOptionPane.ERROR_MESSAGE);
                        player.getItem().setCoin(player.getItem().getCoin()+500);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                    
                    boolean loop = true;
                    
                    do{
                        int randomskill = (int)(Math.random() * 8);
                        if(randomskill == 0){
                            if(enemyPokemon.getMp() >= 25){
                                enemyPokemon.attack1(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 1){
                             if(enemyPokemon.getMp() >= 50){
                                enemyPokemon.attack2(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 2){
                             if(enemyPokemon.getMp() >= 25){
                             enemyPokemon.attack3(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 3){
                             if(enemyPokemon.getMp() >= 100){
                             enemyPokemon.attack4(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 4){
                             enemyPokemon.healHp(15);
                             show2.setText(enemyPokemon.getName() + "use berry");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 5){
                             enemyPokemon.healHp(25);
                             show2.setText(enemyPokemon.getName() + "use potion");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 6){
                             enemyPokemon.healMp(20);
                             show2.setText(enemyPokemon.getName() + "use ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 7){
                             enemyPokemon.healMp(30);
                             show2.setText(enemyPokemon.getName() + "use super ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                    }while(loop);

                    if(myPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                        setVisible(false);
                        new GuiPlay(player);
                    }
               }
               else{
                   JOptionPane.showMessageDialog(null,"Not enough Mana","Wraning",JOptionPane.ERROR_MESSAGE);
               }
           }
        });
        
        skill4.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               if(myPokemon.getMp() >= 100){
                    myPokemon.attack4(myPokemon,enemyPokemon);
                    show1.setText(myPokemon.getName() +" use skill "+ myPokemon.getNameskill4());
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());

                    if(enemyPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,myPokemon.getName() + " win , You got 500 coins","Message",JOptionPane.ERROR_MESSAGE);
                        player.getItem().setCoin(player.getItem().getCoin()+500);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                    
                    boolean loop = true;
                    
                    do{
                        int randomskill = (int)(Math.random() * 8);
                        if(randomskill == 0){
                            if(enemyPokemon.getMp() >= 25){
                                enemyPokemon.attack1(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 1){
                             if(enemyPokemon.getMp() >= 50){
                                enemyPokemon.attack2(enemyPokemon,myPokemon);
                                show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                                show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                                show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                                show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                                show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                                loop = false;
                            }
                         }
                         else if(randomskill == 2){
                             if(enemyPokemon.getMp() >= 25){
                             enemyPokemon.attack3(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 3){
                             if(enemyPokemon.getMp() >= 100){
                             enemyPokemon.attack4(enemyPokemon,myPokemon);
                             show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                            }
                         }
                         else if(randomskill == 4){
                             enemyPokemon.healHp(15);
                             show2.setText(enemyPokemon.getName() + "use berry");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 5){
                             enemyPokemon.healHp(25);
                             show2.setText(enemyPokemon.getName() + "use potion");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 6){
                             enemyPokemon.healMp(20);
                             show2.setText(enemyPokemon.getName() + "use ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                         else if(randomskill == 7){
                             enemyPokemon.healMp(30);
                             show2.setText(enemyPokemon.getName() + "use super ether");
                             show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                             show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                             show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                             show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                             loop = false;
                         }
                    }while(loop);

                    if(myPokemon.getHp() == 0){
                        JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                        setVisible(false);
                        new GuiPlay(player);
                    }
               }
               else{
                   JOptionPane.showMessageDialog(null,"Not enough Mana","Wraning",JOptionPane.ERROR_MESSAGE);
               }
           }
        });
        
        //Phamacy
        
        if(player.getItem().getBerry() == 0){
            berry.setEnabled(false);
        }
        if(player.getItem().getPotion()== 0){
            potion.setEnabled(false);
        }
        if(player.getItem().getEther()== 0){
            ether.setEnabled(false);
        }
        if(player.getItem().getSuperether()== 0){
            superether.setEnabled(false);
        }
        if(player.getItem().getPokeball()== 0){
            pokeball.setEnabled(false);
        }
        
        berry.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
            if(myPokemon.getHp() < myPokemon.getRememberhp()){
               if(player.getItem().getBerry()>0){
                    player.getItem().healberry(myPokemon);
                    berry.setText("Berry : " + player.getItem().getBerry());
                    show1.setText(myPokemon.getName() +" use berry(+15HP) ");
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                    if(player.getItem().getBerry() == 0)
                        berry.setEnabled(false);
               }
               
               int randomskill = (int)(Math.random() * 8);
               if(randomskill == 0){
                    enemyPokemon.attack1(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                }
                else if(randomskill == 1){
                    enemyPokemon.attack2(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                }
                else if(randomskill == 2){
                    enemyPokemon.attack3(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                }
                else if(randomskill == 3){
                    enemyPokemon.attack4(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());   
                }
                else if(randomskill == 4){
                    enemyPokemon.healHp(15);
                    show2.setText(enemyPokemon.getName() + "use berry");
                }
                else if(randomskill == 5){
                    enemyPokemon.healHp(25);
                    show2.setText(enemyPokemon.getName() + "use potion");
                }
                else if(randomskill == 6){
                    enemyPokemon.healMp(20);
                    show2.setText(enemyPokemon.getName() + "use ether");
                }
                else if(randomskill == 7){
                    enemyPokemon.healMp(30);
                    show2.setText(enemyPokemon.getName() + "use super ether");
                }
               
               if(myPokemon.getHp() == 0){
                   JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                   setVisible(false);
                   new GuiPlay(player);
               }
            }else{
                JOptionPane.showMessageDialog(null,"Hp is full","Wraning",JOptionPane.ERROR_MESSAGE);
            }
           }
        });
        
        potion.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
            if(myPokemon.getHp() < myPokemon.getRememberhp()){
               if(player.getItem().getPotion()>0){
                    player.getItem().healpotion(myPokemon);
                    potion.setText("Potion : " + player.getItem().getPotion());
                    show1.setText(myPokemon.getName() +" use potion(+25HP) ");
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                    
                    if(player.getItem().getPotion() == 0)
                        potion.setEnabled(false);
               }
               
               int randomskill = (int)(Math.random() * 8);
               if(randomskill == 0){
                    enemyPokemon.attack1(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                }
                else if(randomskill == 1){
                    enemyPokemon.attack2(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                }
                else if(randomskill == 2){
                    enemyPokemon.attack3(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                }
                else if(randomskill == 3){
                    enemyPokemon.attack4(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());   
                }
                else if(randomskill == 4){
                    enemyPokemon.healHp(15);
                    show2.setText(enemyPokemon.getName() + "use berry");
                }
                else if(randomskill == 5){
                    enemyPokemon.healHp(25);
                    show2.setText(enemyPokemon.getName() + "use potion");
                }
                else if(randomskill == 6){
                    enemyPokemon.healMp(20);
                    show2.setText(enemyPokemon.getName() + "use ether");
                }
                else if(randomskill == 7){
                    enemyPokemon.healMp(30);
                    show2.setText(enemyPokemon.getName() + "use super ether");
                }
               
               if(myPokemon.getHp() == 0){
                   JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                   setVisible(false);
                   new GuiPlay(player);
               }
            }else{
                JOptionPane.showMessageDialog(null,"Hp is full","Wraning",JOptionPane.ERROR_MESSAGE);
            }
           }
        });
        
        ether.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
            if(myPokemon.getMp() < myPokemon.getRemembermp()){
               if(player.getItem().getEther()>0){
                    player.getItem().healether(myPokemon);
                    ether.setText("Ether : " + player.getItem().getEther());
                    show1.setText(myPokemon.getName() +" use ether(+20MP) ");
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                    
                    if(player.getItem().getEther() == 0)
                        ether.setEnabled(false);
               }
               
               int randomskill = (int)(Math.random() * 8);
               if(randomskill == 0){
                    enemyPokemon.attack1(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                }
                else if(randomskill == 1){
                    enemyPokemon.attack2(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                }
                else if(randomskill == 2){
                    enemyPokemon.attack3(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                }
                else if(randomskill == 3){
                    enemyPokemon.attack4(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());   
                }
                else if(randomskill == 4){
                    enemyPokemon.healHp(15);
                    show2.setText(enemyPokemon.getName() + "use berry");
                }
                else if(randomskill == 5){
                    enemyPokemon.healHp(25);
                    show2.setText(enemyPokemon.getName() + "use potion");
                }
                else if(randomskill == 6){
                    enemyPokemon.healMp(20);
                    show2.setText(enemyPokemon.getName() + "use ether");
                }
                else if(randomskill == 7){
                    enemyPokemon.healMp(30);
                    show2.setText(enemyPokemon.getName() + "use super ether");
                }
               
               if(myPokemon.getHp() == 0){
                   JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                   setVisible(false);
                   new GuiPlay(player);
               }
            }else{
                JOptionPane.showMessageDialog(null,"Mp is full","Wraning",JOptionPane.ERROR_MESSAGE);
            }
           }
        });
        
        superether.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
            if(myPokemon.getMp() < myPokemon.getRemembermp()){
               if(player.getItem().getSuperether()>0){
                    player.getItem().healsuperether(myPokemon);
                    superether.setText("Superether : " + player.getItem().getSuperether());
                    show1.setText(myPokemon.getName() +" use super ether(+30MP) ");
                    show3.setText("HP : " + myPokemon.getHp() + "/" + myPokemon.getRememberhp());
                    show4.setText("HP : " + enemyPokemon.getHp() + "/" + enemyPokemon.getRememberhp());
                    show5.setText("MP : " + myPokemon.getMp()+ "/" + myPokemon.getRemembermp());
                    show6.setText("MP : " + enemyPokemon.getMp()+ "/" + enemyPokemon.getRemembermp());
                    
                    if(player.getItem().getSuperether() == 0)
                        superether.setEnabled(false);
               }
               
               int randomskill = (int)(Math.random() * 8);
               if(randomskill == 0){
                    enemyPokemon.attack1(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill1());
                }
                else if(randomskill == 1){
                    enemyPokemon.attack2(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill2());
                }
                else if(randomskill == 2){
                    enemyPokemon.attack3(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill3());
                }
                else if(randomskill == 3){
                    enemyPokemon.attack4(enemyPokemon,myPokemon);
                    show2.setText(enemyPokemon.getName() +" use "+ enemyPokemon.getNameskill4());   
                }
                else if(randomskill == 4){
                    enemyPokemon.healHp(15);
                    show2.setText(enemyPokemon.getName() + "use berry");
                }
                else if(randomskill == 5){
                    enemyPokemon.healHp(25);
                    show2.setText(enemyPokemon.getName() + "use potion");
                }
                else if(randomskill == 6){
                    enemyPokemon.healMp(20);
                    show2.setText(enemyPokemon.getName() + "use ether");
                }
                else if(randomskill == 7){
                    enemyPokemon.healMp(30);
                    show2.setText(enemyPokemon.getName() + "use super ether");
                }
               
               if(myPokemon.getHp() == 0){
                   JOptionPane.showMessageDialog(null,enemyPokemon.getName() + " win","Message",JOptionPane.ERROR_MESSAGE);
                   setVisible(false);
                   new GuiPlay(player);
               }
            }else{
                JOptionPane.showMessageDialog(null,"Mp is full","Wraning",JOptionPane.ERROR_MESSAGE);
            }
           }
        });
        
        pokeball.addActionListener( new ActionListener(){
           public void actionPerformed(ActionEvent e) {
                int count = 0;
                if(player.getItem().getPokeball()>0){
                    for(Pokemon p : player.getBag()){
                        count++;
                    }
                    
                    if(count < 8){
                        boolean check = player.getItem().catchpokemon(player.getBag(),myPokemon,enemyPokemon);
                        pokeball.setText("Pokeball : " + player.getItem().getPokeball());
                        if(check){
                            JOptionPane.showMessageDialog(null,"Catch " + enemyPokemon.getName() + " Sucess","Message",JOptionPane.ERROR_MESSAGE);
                            setVisible(false);
                            new GuiPlay(player);
                        }
                        else{
                            JOptionPane.showMessageDialog(null,"Catch " + enemyPokemon.getName() + " Fail","Message",JOptionPane.ERROR_MESSAGE);
                        }
                        
                        if(player.getItem().getPokeball() == 0)
                            pokeball.setEnabled(false);
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Bag IS Full","Message",JOptionPane.ERROR_MESSAGE);
                    }
                }
           }
        });
        
        poke[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(0),enemyPokemon,number);
            }
        });
        
        poke[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(1),enemyPokemon,number);
            }
        });
        
        poke[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(2),enemyPokemon,number);
            }
        });
        
        poke[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(3),enemyPokemon,number);
            }
        });
        
        poke[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(4),enemyPokemon,number);
            }
        });
        
        poke[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(5),enemyPokemon,number);
            }
        });
        
        poke[6].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(6),enemyPokemon,number);
            }
        });
    
        poke[7].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiBattleGround(player,player.getBag().get(7),enemyPokemon,number);
            }
        });
        
        run.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiPlay(player );
            }
        });
        
        
    }
}
