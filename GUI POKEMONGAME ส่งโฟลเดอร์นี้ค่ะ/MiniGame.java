
import java.text.DecimalFormat;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class MiniGame extends JFrame{

    int x = (int)(Math.random()*100)+1;
    int y = (int)(Math.random()*100)+1;
    private Trainer player;

    private JTextField ans;
    private int ansnum;
    private int result;

    public MiniGame(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,400);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

        this.player = player;

        Container c = getContentPane();

        ImageIcon bg = new ImageIcon("bgminigame.png");
        JLabel background = new JLabel(bg);
        
        c.add(background);
        JLabel adding = new JLabel("" + x + " + " + y + " = ?");
        adding.setBounds(150, 5, 500, 90);
        adding.setFont(new Font("Feast of Flesh BB", Font.BOLD, 50));
        background.add(adding);

        final JTextField ans = new JTextField(10);
        ans.setBounds(320, 125, 120, 25);

        background.add(ans);

        JButton addans = new JButton("ANS");
        addans.setBounds(320, 160, 120, 25);

        background.add(addans);

        addans.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try{
                    ansnum = Integer.parseInt(ans.getText());
                    result = x + y;
                    if(ansnum == result){
                        player.getItem().setCoin(player.getItem().getCoin() + 100);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Your answer is wrong","Warning",JOptionPane.ERROR_MESSAGE);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(null,"You must enter integer","Warning",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}