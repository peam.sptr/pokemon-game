
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class GuiPlay extends JFrame{
    
    
    public GuiPlay(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
        
        Choice choice = new Choice();
        Container c = getContentPane();
        
        ImageIcon bg = new ImageIcon("bgmode.png");
        ImageIcon bt1 = new ImageIcon("pokedex.png");
        ImageIcon bt2 = new ImageIcon("battle.png");
        ImageIcon bt3 = new ImageIcon("shop.png");
        ImageIcon bt4 = new ImageIcon("pokemoncenter.png");
        ImageIcon bt5 = new ImageIcon("quit.png");
        ImageIcon bt6 = new ImageIcon("minigame.png");

        JLabel backgroung = new JLabel(bg);
        c.add(backgroung);
        
        JButton pokedex = new JButton();
        JButton battle = new JButton();
        JButton shop = new JButton();
        JButton pokemoncenter = new JButton();
        JButton quit = new JButton();
        JButton minigame = new JButton();

        // pokedex.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        // battle.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        // shop.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        // pokemoncenter.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        // quit.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));

        pokedex.setBounds(120, 425, 200, 40);
        battle.setBounds(465, 425, 200, 40);
        shop.setBounds(805, 425, 200, 40);
        pokemoncenter.setBounds(1100, 425, 240, 40);
        quit.setBounds(660, 750, 200, 40);
        minigame.setBounds(880, 750, 40, 40);

        pokedex.setIcon(bt1);
        battle.setIcon(bt2);
        shop.setIcon(bt3);                
        pokemoncenter.setIcon(bt4);                
        quit.setIcon(bt5);
        minigame.setIcon(bt6);

        backgroung.add(pokedex);
        backgroung.add(battle);
        backgroung.add(shop);
        backgroung.add(pokemoncenter);
        backgroung.add(quit);
        backgroung.add(minigame);
        
        JLabel berry = new JLabel("Berry: " + player.getItem().getBerry(),JLabel.CENTER);
        JLabel potion = new JLabel("Potion: " + player.getItem().getPotion(),JLabel.CENTER);
        JLabel ether = new JLabel("Ether: " + player.getItem().getEther(),JLabel.CENTER);
        JLabel superether = new JLabel("Super Ether: " + player.getItem().getSuperether(),JLabel.CENTER);
        JLabel pokeball = new JLabel("Pokeball: " + player.getItem().getPokeball(),JLabel.CENTER);
        JLabel coin = new JLabel("Coin: " + player.getItem().getCoin(),JLabel.CENTER);

        berry.setBounds(140, 615, 200, 40);
        potion.setBounds(330, 615, 200, 40);
        ether.setBounds(535, 615, 200, 40);
        superether.setBounds(755, 615, 200, 40);
        pokeball.setBounds(983, 615, 200, 40);
        coin.setBounds(1197, 615, 200, 40);

        berry.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        potion.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        ether.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        superether.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        pokeball.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        coin.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        
        backgroung.add(berry);
        backgroung.add(potion);
        backgroung.add(ether);
        backgroung.add(superether);                        
        backgroung.add(pokeball);
        backgroung.add(coin);
        
        pokedex.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){              
                new GuiTrainerStatus(player.getBag());
            }
        });
        
        
        battle.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuichooseEnemyPokemonBattle(player);
            }
        });
        
        
        shop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiShop(player);
            }
        });
        
        pokemoncenter.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                choice.feed(player.getBag());
                JOptionPane.showMessageDialog(null,"Successfully Heal All Pokemon You Can Look Status Of Your Pokemon In Pokedex");
            }
        });
        
        quit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new PokemonGame();
            }
        });

        minigame.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new MiniGame(player);
            }
        });

        pack();
        setLocationRelativeTo(null);
    }
}
