
package pokemongame;

public interface Skillrock {
    void rockthrow(Pokemon myPokemon,Pokemon enemy);
    void rockslide(Pokemon myPokemon,Pokemon enemy);
}
