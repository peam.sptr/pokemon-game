
package pokemongame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class GuiTrainer extends JFrame{
    private Trainer player;
    private ArrayList<Pokemon> bag;
    private int number = 1;
    
    
    
    public GuiTrainer(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1450,850);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        
        this.player = player;
        this.bag = player.getBag();
        
        Container c = getContentPane();
        
        c.setLayout(new BorderLayout());
//        c.setBackground(Color.WHITE);
        
        
        JPanel head = new JPanel();
        head.setLayout(new GridLayout(2,1,0,30));
        
        JLabel title = new JLabel("Choose Pokemon",JLabel.CENTER);
        title.setFont(new Font("Eras Demi ITC", Font.BOLD, 50));
        
        head.add(title);
        
        JPanel skill = new JPanel();
        skill.setLayout(new GridLayout(1,4));
           
        JLabel skill1 = new JLabel("skill1-Magical Leaf: 20HP,25MP",JLabel.CENTER);
        JLabel skill2 = new JLabel("skill2-Solar Beam: 25HP,50MP",JLabel.CENTER);
        JLabel skill3 = new JLabel("skill3-Tackle: 15HP,25MP",JLabel.CENTER);
        JLabel skill4 = new JLabel("skill4-Growl: 40Hp,100MP",JLabel.CENTER);
        
        skill1.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        skill2.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        skill3.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        skill4.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        
        skill.add(skill1);
        skill.add(skill2);
        skill.add(skill3);
        skill.add(skill4);
        
        head.add(skill);
        
        c.add(head,BorderLayout.NORTH);
        
        JPanel choose = new JPanel();
        choose.setLayout(new GridLayout(2,1));
        
        JPanel pokemon = new JPanel();
        pokemon.setLayout(new FlowLayout());
        
        JButton poke1 = new JButton("Chicolita");
        poke1.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        JButton poke2 = new JButton("Cyndaquil");
        poke2.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        JButton poke3 = new JButton("Totodile");
        poke3.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        JButton poke4 = new JButton("Pikachu");
        poke4.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        JButton poke5 = new JButton("Golduck");
        poke5.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        poke5.setBounds(700,650,120,40);
        
        pokemon.add(poke1);
        pokemon.add(poke2);
        pokemon.add(poke3);
        pokemon.add(poke4);
        pokemon.add(poke5);
        
        JButton ok = new JButton("OK");
        choose.add(pokemon);
        choose.add(ok);
        
        c.add(choose,BorderLayout.SOUTH);
        
        ImageIcon chikorita = new ImageIcon("chikorita.png");
        ImageIcon cyndaquil = new ImageIcon("cyndaquil.png");
        ImageIcon totodile = new ImageIcon("totodile.png");
        ImageIcon pikachu = new ImageIcon("pikachu.png");
        ImageIcon golduck = new ImageIcon("golduck.png");
        
        JLabel img;
        img = new JLabel(chikorita);
        c.add(img,BorderLayout.CENTER);
        
        poke1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                skill1.setText("skill1-Magical Leaf: 20HP,25MP");
                skill2.setText("skill2-Solar Beam: 25HP,50MP");
                skill3.setText("skill3-Tackle: 15HP,25MP");
                skill4.setText("skill4-Growl: 40Hp,100MP");
                img.setIcon(chikorita);
                number = 1;
            }
        });
        
        poke2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                skill1.setText("skill1-Burn Up: 20HP,25MP");
                skill2.setText("skill2-Inferno: 25HP,50MP");
                skill3.setText("skill3-Tackle: 15HP,25MP");
                skill4.setText("skill4-Rock Throw: 40Hp,100MP");
                img.setIcon(cyndaquil);
                number = 2;
            }
        });
        
        poke3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                skill1.setText("skill1-Water Gun: 20HP,25MP");
                skill2.setText("skill2-Hydro Pump: 25HP,50MP");
                skill3.setText("skill3-Aqua Tail: 15HP,25MP");
                skill4.setText("skill4-Growl: 40Hp,100MP");
                img.setIcon(totodile);
                number = 3;
            }
        });
        
        poke4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                skill1.setText("skill1-Charm: 20HP,25MP");
                skill2.setText("skill2-Sweet kiss: 25HP,50MP");
                skill3.setText("skill3-Thunder Shock: 15HP,25MP");
                skill4.setText("skill4-Thunder Bolt: 40Hp,100MP");
                img.setIcon(pikachu);
                number = 4;
            }
        });
        
        poke5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                skill1.setText("skill1-Water Gun: 20HP,25MP");
                skill2.setText("skill2-Hydro Pump: 25HP,50MP");
                skill3.setText("skill3-Aqua Tail: 15HP,25MP");
                skill4.setText("skill4-Tackle: 40Hp,100MP");
                img.setIcon(golduck);
                number = 5;
            }
        });
        
        ok.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(number == 1){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Chikorita(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 2){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Cyndaquil(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 3){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Totodile(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 4){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Pikachu(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }
                }
                else if(number == 5){
                    String namepoke = JOptionPane.showInputDialog(null, "Name of your pokemon:", "SET NAME", JOptionPane.PLAIN_MESSAGE);
                    if(namepoke != null){
                        bag.add(new Golduck(namepoke));
                        player.setBag(bag);
                        setVisible(false);
                        new GuiPlay(player);
                    }                    
                }
        
            }
        });
        
        
//        c.add(chi,BorderLayout.CENTER);
        
//        pack();
        
    }
    
    
    
}
