
package pokemongame;

import java.util.*;

public class Battlepokemon {
    private Scanner sc;
    public void battle(ArrayList<Pokemon> bag,Item item) {
        Choice choice = new Choice();
        sc = new Scanner(System.in);
        ArrayList<Pokemon> pokemons = Randompokemon.getPokemons(5);
        System.out.println("\n\nPokemon around you");
        int no = 0;
        choice.printstatus(pokemons);
        System.out.print("\nSelect pokemon number or run(-1): ");
        no = sc.nextInt();
        no = no-1;
        if(no < 0){
            sc.nextLine();
            return;
        }
        Pokemon wildPokemon = pokemons.get(no);
        System.out.println("\n\nPokemon in bag: ");
        choice.printstatus(bag);
        System.out.print("\nSelect pokemon in bag: ");
        
        no = sc.nextInt();
        no = no-1;
        Pokemon myPokemon = bag.get(no);

        boolean isWin = false;
        boolean check = false;
        do{
            System.out.println("\nChoice");
            System.out.println("1.Attack");
            System.out.println("2.Use Item");
            System.out.println("3.Change Pokemon");
            System.out.println("4.Run");
            System.out.print("Enter number: ");
            no = sc.nextInt();

            if(no == 1){
                System.out.println("\nAttack");
                do{
                    System.out.println("\nChoose Skill");
                    myPokemon.printattack1();
                    myPokemon.printattack2();
                    myPokemon.printattack3();
                    myPokemon.printattack4();
                    System.out.print("\nEnter number: ");
                    no = sc.nextInt();
                    if(no == 1){
                        myPokemon.attack1(myPokemon,wildPokemon);
                        break;
                    }
                    else if(no == 2){
                        myPokemon.attack2(myPokemon,wildPokemon);
                        break;
                    }
                    else if(no == 3){
                        myPokemon.attack3(myPokemon,wildPokemon);
                        break;
                    }
                    else if(no == 4){
                        myPokemon.attack4(myPokemon,wildPokemon);
                        break;
                    }
                    else{
                        System.out.println("Don't have number choose again");
                    }
                }while(true);

                if (wildPokemon.getHp() == 0){
                    isWin = true;
                    break;
                }

                int randomskill = (int)(Math.random() * 8);
                if(randomskill == 0){
                    System.out.print("\nIt use skill: ");
                    wildPokemon.printattack1();
                    wildPokemon.attack1(wildPokemon,myPokemon);
                }
                else if(randomskill == 1){
                    System.out.print("\nIt use skill: ");
                    wildPokemon.printattack2();
                    wildPokemon.attack2(wildPokemon,myPokemon);
                }
                else if(randomskill == 2){
                    System.out.print("\nIt use skill: ");
                    wildPokemon.printattack3();
                    wildPokemon.attack3(wildPokemon,myPokemon);
                }
                else if(randomskill == 3){
                    System.out.print("\nIt use skill: ");
                    wildPokemon.printattack4();
                    wildPokemon.attack4(wildPokemon,myPokemon);
                }
                else if(randomskill == 4){
                    System.out.print("\nIt use berry(+15HP)");
                    wildPokemon.healHp(15);
                }
                else if(randomskill == 5){
                    System.out.print("\nIt use potion(+25HP)");
                    wildPokemon.healHp(25);
                }
                else if(randomskill == 6){
                    System.out.print("\nIt use ether(+20MP)");
                    wildPokemon.healMp(20);
                }
                else if(randomskill == 7){
                    System.out.print("\nIt use super ether(+30MP)");
                    wildPokemon.healMp(30);
                }

                if (myPokemon.getHp() == 0){
                    isWin = false;
                    break;
                }
            }
            else if(no == 2){
                System.out.println("\nUse Item");
                do{
                    System.out.println("1.Berry (+15HP): " + item.getBerry());
                    System.out.println("2.Potion (+25HP): " + item.getPotion());
                    System.out.println("3.Ether (+20MP): " + item.getEther());
                    System.out.println("4.Super Ether (+30MP): " + item.getSuperether());
                    System.out.println("5.Pokeball (+30MP): " + item.getPokeball());
                    System.out.print("\nEnter number or back(-1): ");
                    no = sc.nextInt();

                    if(no == 1){
                        if(item.getBerry() > 0){
                            item.healberry(myPokemon);
                            break;
                        }
                        else{
                            System.out.println("Berry not enough choose again");
                        }
                    }
                    else if(no == 2){
                        if(item.getPotion() > 0){
                            item.healpotion(myPokemon);
                            break;
                        }
                        else{
                            System.out.println("Potion not enough choose again");
                        }
                        
                    }
                    else if(no == 3){
                        if(item.getEther() > 0){
                            item.healether(myPokemon);
                            break;
                        }
                        else{
                            System.out.println("Ether not enough choose again");
                        }
                    }
                    else if(no == 4){
                        if(item.getSuperether() > 0){
                            item.healsuperether(myPokemon);
                            break;
                        }
                        else{
                            System.out.println("Superether not enough choose again");
                        }
                    }
                    else if(no == 5){
                        if(item.getPokeball() > 0){
                            check = item.catchpokemon(bag,myPokemon,wildPokemon);
                            break;
                        }
                        else{
                            System.out.println("Pokeball not enough choose again");
                        }
                    }
                    else if(no == -1){
                        break;
                    }
                    else{
                        System.out.println("Don't have number choose again");
                    }
                }while(true);

                if(check){
                    isWin = check;
                    break;
                }
            }
            else if(no == 3){
                System.out.println("\nChange Pokeball");
                System.out.println("\nPokemon in bag: ");
                choice.printstatus(bag);
                System.out.print("\nSelect pokemon in bag: ");
                            
                no = sc.nextInt();
                no = no-1;
                myPokemon = bag.get(no);
                System.out.println(" " + myPokemon + " HP: " + myPokemon.getHp() + " MP: " + myPokemon.getMp());
            }
            else if(no == 4){
                System.out.println("Run");
                break;
            }
        }while(true);
        
        if(isWin){
            System.out.println(myPokemon.getName() + " win");
            int coin = item.getCoin();
            item.setCoin(coin+500);
        }
        else{
            System.out.println(wildPokemon.getName() + " win");
        }
        sc.nextLine();
    }
}
