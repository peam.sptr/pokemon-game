
package pokemongame;

import java.util.*;

public class Trainer {
    private ArrayList<Pokemon> bag;
    private Scanner sc;
    private Item item = new Item();
    private String name;

    public Trainer(String name){
        bag = new ArrayList<Pokemon>();
        this.name = name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public void setBag(ArrayList<Pokemon> bag){
        this.bag = bag;
    }
    
    public ArrayList<Pokemon> getBag(){
        return bag;
    }
    
    public void setItem(Item item){
        this.item = item;
    }
    
    public Item getItem(){
        return item;
    }
    
    public void play(){
        // System.out.println("Debug");
        Choice choice = new Choice();
        Battlepokemon battlepokemon = new Battlepokemon();
        String cmd = "";
        do{
            System.out.println("\n1.Pokedex");
            System.out.println("2.Battle");
            System.out.println("3.Buy Item");
            System.out.println("4.Pokemon center[feed]");
            System.out.println("5.Quit");
            System.out.print("Enter cmd: ");
            cmd = sc.nextLine();
            System.out.println("");
            if(cmd.equals("1")){
                System.out.println("Status of my pokemon");
                choice.trainerstatus(bag,item);
            }
            else if(cmd.equals("2")){
                System.out.println("Battle pokemon");
                battlepokemon.battle(bag,item);
            }
            else if(cmd.equals("3")){
                System.out.println("Buy Item");
                choice.buy(item);
            }
            else if(cmd.equals("4")){
                System.out.println("POKEMON CENTER [FEED]");
                choice.feed(bag);
            }
        }while(true);
    }
}
