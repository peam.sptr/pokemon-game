
package pokemongame;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;

public class GuiPlay extends JFrame{
    
    private Trainer player;
    private ArrayList<Pokemon> bag;
    private Item item;
    
    public GuiPlay(Trainer player){
        super("POKEMON GAME");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1450,850);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        
        this.player = player;
        this.bag = player.getBag();
        this.item = player.getItem();
        
        Container c = getContentPane();
        
        c.setLayout(new BorderLayout());
        
        JLabel title = new JLabel("Mode",JLabel.CENTER);
        title.setFont(new Font("Eras Demi ITC", Font.BOLD, 50));
        c.add(title,BorderLayout.NORTH);
        
        JPanel box = new JPanel();
        box.setLayout(new GridLayout(3,3,0,0));
        
        ImageIcon show1 = new ImageIcon("pokedex.png");
        ImageIcon show2 = new ImageIcon("battle.png");
        ImageIcon show3 = new ImageIcon("shop.png");
        ImageIcon show4 = new ImageIcon("pokemoncenter.png");
        ImageIcon show5 = new ImageIcon("quit.png");
        ImageIcon show6 = new ImageIcon("gap1.png");
        ImageIcon show7 = new ImageIcon("gap2.png");
        ImageIcon show8 = new ImageIcon("berry.png");
        ImageIcon show9 = new ImageIcon("potion.png");
        ImageIcon show10 = new ImageIcon("ether.png");
        ImageIcon show11 = new ImageIcon("superether.png");
        ImageIcon show12 = new ImageIcon("pokeball.png");
        ImageIcon show13 = new ImageIcon("coin.png");
        
        
        JLabel gap1 = new JLabel();
        gap1.setIcon(show6);
        JLabel gap2 = new JLabel();
        gap2.setIcon(show7);
        
        JButton pokedex = new JButton("1.Pokedex");
        pokedex.setIcon(show1);
        pokedex.setFont(new Font("Eras Demi ITC", Font.BOLD, 20));
        
        JButton battle = new JButton("2.Battle");
        battle.setIcon(show2);
        battle.setFont(new Font("Eras Demi ITC", Font.BOLD, 15));
        
        JButton shop = new JButton("3.Shop");
        shop.setIcon(show3);
        shop.setFont(new Font("Eras Demi ITC", Font.BOLD, 15));
        
        JButton pokemoncenter = new JButton("4.Pokemon Center");
        pokemoncenter.setIcon(show4);
        pokemoncenter.setFont(new Font("Eras Demi ITC", Font.BOLD, 15));
        
        JButton quit = new JButton("5.Quit");
        quit.setIcon(show5);
        quit.setFont(new Font("Eras Demi ITC", Font.BOLD, 15));
        
        JPanel itemdex1 = new JPanel();
        itemdex1.setLayout(new GridLayout(2,2));
        
        JLabel berry = new JLabel("Berry: " + item.getBerry(),JLabel.CENTER);
        JLabel potion = new JLabel("Potion: " + item.getPotion(),JLabel.CENTER);
        JLabel ether = new JLabel("Ether: " + item.getEther(),JLabel.CENTER);
        JLabel superether = new JLabel("Super Ether: " + item.getSuperether(),JLabel.CENTER);
        
        berry.setIcon(show8);
        potion.setIcon(show9);
        ether.setIcon(show10);
        superether.setIcon(show11);
        
        itemdex1.add(berry);
        itemdex1.add(potion);
        itemdex1.add(ether);
        itemdex1.add(superether);
        
        JPanel itemdex2 = new JPanel();
        itemdex2.setLayout(new GridLayout(1,2));
        
        JLabel pokeball = new JLabel("Pokeball: " + item.getPokeball(),JLabel.CENTER);
        JLabel coin = new JLabel("Coin: " + item.getCoin(),JLabel.CENTER);
        
        pokeball.setIcon(show12);
        coin.setIcon(show13);
        
        itemdex2.add(pokeball);
        itemdex2.add(coin);
        
        box.add(pokedex);
        box.add(gap1);
        box.add(battle);
        box.add(shop);
        box.add(gap2);
        box.add(pokemoncenter);
        box.add(itemdex1);
        box.add(quit);
        box.add(itemdex2);
        
        c.add(box,BorderLayout.CENTER);

        bag.add(new Pikachu("Hey"));
        bag.add(new Golduck("Hello"));
        
        pokedex.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
//                setVisible(false);                  
                    new GuiTrainerStatus(bag);
            }
        });
        
        
        //Battle not have tee
        
        
        shop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                new GuiShop(player.getItem());
                
            }
        });
        
    }
    
    
    
}
