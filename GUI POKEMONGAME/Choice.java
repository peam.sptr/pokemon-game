
package pokemongame;

import java.util.*;

public class Choice {
    private Scanner sc;
    
    public void printstatus(ArrayList<Pokemon> bag){
        int number = 1;
        for(Pokemon p : bag){
            System.out.println("" + number + " " + p + " HP: " + p.getHp() + " MP: " + p.getMp());
            number++;
        }
    }

    public void trainerstatus(ArrayList<Pokemon> bag,Item item){
        int number = 1;
        for(Pokemon p : bag){
            System.out.println("" + number + " " + p + " HP: " + p.getHp() + " MP: " + p.getMp());
            number++;
        }
        System.out.println("Item in bag");
        System.out.println("1.Berry (+15HP): " + item.getBerry());
        System.out.println("2.Potion (+25HP): " + item.getPotion());
        System.out.println("3.Ether (+20MP): " + item.getEther());
        System.out.println("4.Super Ether (+30MP): " + item.getSuperether());
        System.out.println("5.Pokeball (+30MP): " + item.getPokeball()); 
        System.out.println("6.Coin: " + item.getCoin());       
    }

    public void buy(Item item){
        System.out.println("1.Berry (+15HP): 25 coin");
        System.out.println("2.Potion (+25HP): 45 coin");
        System.out.println("3.Ether (+20MP): 35 coin");
        System.out.println("4.Super Ether (+30MP): 50 coin");
        System.out.println("5.Pokeball (+30MP): 100 coin");

        sc = new Scanner(System.in);
        System.out.print("Enter number or back(-1): ");
        int no = 0;
        no = sc.nextInt();
        
        do{
            if(no == 1){
                if(item.getCoin() >= 25){
                    item.setCoin(item.getCoin()-25);
                    item.setBerry(item.getBerry()+1);
                    break;
                }
                else{
                    System.out.println("Not enough coin");
                    break;
                }
            }
            else if(no == 2){
                if(item.getCoin() >= 45){
                    item.setCoin(item.getCoin()-45);
                    item.setPotion(item.getPotion()+1);
                    break;
                }
                else{
                    System.out.println("Not enough coin");
                    break;
                }
            }
            else if(no == 3){
                if(item.getCoin() >= 35){
                    item.setCoin(item.getCoin()-35);
                    item.setEther(item.getEther()+1);
                    break;
                }
                else{
                    System.out.println("Not enough coin");
                    break;
                }

            }
            else if(no == 4){
                if(item.getCoin() >= 45){
                    item.setCoin(item.getCoin()-50);
                    item.setSuperether(item.getSuperether()+1);
                    break;
                }
                else{
                    System.out.println("Not enough coin");
                    break;
                }
            }
            else if(no == 5){
                if(item.getCoin() >= 100){
                    item.setCoin(item.getCoin()-100);
                    item.setPokeball(item.getPokeball()+1);
                    break;
                }
                else{
                    System.out.println("Not enough coin");
                    break;
                }
            }
            else if(no == -1){
                return;
            }
            else{
                System.out.println("Don't have number choose again");
                break;
            }
        }while(true);
    }

    public void feed(AbstractList<Pokemon> bag){
        for(Pokemon p : bag){
            p.setHp(p.getRememberhp());
            p.setMp(p.getRemembermp());
        }
    }
}
